#!/usr/bin/env bash
#
# Configurar bash-libs no seu sitema
#


__version__='2.1'
__author__='Bruno Chaves'
__file__=$(readlink -f $0)
dir_for_project=$(dirname $__file__)
work_dir=$(pwd)

cd $dir_for_project
source ./init.sh

if [[ $(id -u) == 0 ]]; then
	out_dir='/usr/local/lib/bash'
	bash_file='/etc/bash.bashrc'
else
	out_dir=~/.local/lib/bash
	bash_file=~/.bashrc
fi

function uninstall()
{
	question "Deseja apagar o diretório $out_dir" || return $?
	rm -rf $out_dir
}

function install()
{
	echo -e "Intalando shell-libs em ... $out_dir"

	cp -u ./string.sh "$out_dir"/
	cp -u ./print_text.sh "$out_dir"/
	cp -u ./utils.sh "$out_dir"/
	cp -u ./sys.sh "$out_dir"/
	cp -u ./crypto.sh "$out_dir"/
	cp -u ./requests.sh "$out_dir"/
	cp -u ./init.sh "$out_dir"/
	cp -u ./pkgmanager.sh "$out_dir"/
}


function _config()
{
	grep "^export BASH_LIBS=.*/lib" "$bash_file" 1> /dev/null && return 0
	echo "Configurando BASH_LIBS em $bash_file"

	echo -e "export BASH_LIBS=$out_dir" >> "$bash_file"
}

function main()
{
	if [[ $1 == 'uninstall' ]]; then
		uninstall
		return 0
	fi

	mkdir -p $out_dir
	install
	_config

	configBashRc
	configZshRc
	cd $work_dir
}



main $@
