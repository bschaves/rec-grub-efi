#!/usr/bin/env bash
#
# Importar as seguintes libs:
#   string.sh
#   print_text.sh
#   utils.sh
#   sys.sh
#   requests.sh
#
#

export STATUS_OUTPUT=0 # int
export SHELL_STATUS_FILE=$(mktemp) # file
export SHELL_DEVICE_FILE=$(mktemp) # file

#export BASH_LIBS=~/.local/lib/bash
#export BASH_LIBS='/usr/local/lib/bash'
#mkdir -p "/tmp/$(whoami)"

[[ -z $HOME ]] && HOME=~/

source ~/.bashrc


# Inserir ~/.local/bin em PATH se não existir.
echo "$PATH" | grep -q "$HOME/.local/bin" || {
	export PATH="$HOME/.local/bin:$PATH"
}



function checkLibs(){
	# Verificar se todas as libs estão disponíveis para importação com o comando source.

	# string.sh
	if [[ ! -f ./string.sh ]] && [[ ! -f "$BASH_LIBS"/string.sh ]]; then
		echo -e "ERRO ... string.sh não encontrado."
		return 1
	fi

	# print_text.sh
	if [[ ! -f ./print_text.sh ]] && [[ ! -f "$BASH_LIBS"/print_text.sh ]]; then
		echo -e "ERRO ... print_text.sh não encontrado."
		return 1
	fi


	# utils.sh
	if [[ ! -f ./utils.sh ]] && [[ ! -f "$BASH_LIBS"/utils.sh ]]; then
		echo -e "ERRO ... utils.sh não encontrado."
		return 1
	fi

	# sys.sh
	if [[ ! -f ./sys.sh ]] && [[ ! -f "$BASH_LIBS"/sys.sh ]]; then
		echo -e "ERRO ... sys.sh não encontrado."
		return 1
	fi

	# requests.sh
	if [[ ! -f ./requests.sh ]] && [[ ! -f "$BASH_LIBS"/requests.sh ]]; then
		echo -e "ERRO ... requests.sh não encontrado."
		return 1
	fi

}


checkLibs || exit 1

source ./string.sh 1>> "$SHELL_DEVICE_FILE" 2>&1 || source "$BASH_LIBS"/string.sh
source ./print_text.sh 1>> "$SHELL_DEVICE_FILE" 2>&1 || source "$BASH_LIBS"/print_text.sh
source ./utils.sh 1>> "$SHELL_DEVICE_FILE" 2>&1 || source "$BASH_LIBS"/utils.sh
source ./sys.sh 1>> "$SHELL_DEVICE_FILE" 2>&1 || source "$BASH_LIBS"/sys.sh
source ./crypto.sh 1>> "$SHELL_DEVICE_FILE" 2>&1 || source "$BASH_LIBS"/crypto.sh
source ./requests.sh 1>> "$SHELL_DEVICE_FILE" 2>&1 || source "$BASH_LIBS"/requests.sh
	